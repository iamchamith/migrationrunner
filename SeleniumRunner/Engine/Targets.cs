﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace SeleniumRunner.Engine
{
    public class Targets
    {
        private List<List<string>> _targets;
        public Targets(object targets)
        {
            _targets = targets as List<List<string>>;
            if (_targets == null || _targets.Count == 0)
            {
                throw new IndexOutOfRangeException();
            }
        }
        public List<By> GetByies()
        {
            var byies = new List<By>();
            foreach (var target in _targets)
            {
                var targetType = target[0].Split('=')[0];
                var targetValue = target[0].Split('=')[1];
                switch (targetType)
                {
                    case "id":
                        byies.Add(By.Id(targetValue));
                        break;
                    case "name":
                        byies.Add(By.Name(targetValue));
                        break;
                    case "css":
                        byies.Add(By.CssSelector(targetValue));
                        break;
                    case "xpath":
                        byies.Add(By.XPath(targetValue)); ;
                        break;
                    default:
                        break;
                }
            }

            return byies;
        }
    }
}
