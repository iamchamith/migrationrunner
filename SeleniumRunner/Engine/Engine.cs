﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumRunner.Model;

namespace SeleniumRunner.Engine
{
    public class Engine
    {
        public SeleniumIde _selenies;
        private IWebDriver _driver;
        public Engine(IWebDriver webDriver, string path)
        {
            _driver = webDriver;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                _selenies = JsonConvert.DeserializeObject<SeleniumIde>(json);
            }
        }

        public Engine Open()
        {
            _driver.Navigate().GoToUrl("http://localhost:5051/#/login");
            _driver.Manage().Window.Maximize();
            return this;
        }

        public Engine Execute()
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            foreach (var test in _selenies.tests)
            {
                foreach (var command in test.commands)
                {
                    
                    _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    if (!new List<string>() { "type", "click" }.ToList().Contains(command.command))
                    {
                        continue;
                    }
                    Console.WriteLine($"{command.targets[0][0].Split('=')[1]}");
                    var targets = new List<By>();
                    By by = null;
                    IWebElement we = null;
                    try
                    {
                        targets = new Targets(command.targets).GetByies();
                    }
                    catch (IndexOutOfRangeException e)
                    {
                        continue;
                    }
                    foreach (var target in targets)
                    {
                        we = _driver.FindElement(target);
                        if (we != null)
                        {
                            by = target;
                            break;
                        }
                    }
                    wait.Until(ExpectedConditions.ElementIsVisible(by));
                    switch (command.command)
                    {
                        case "type":
                            we?.SendKeys(command.value);
                            Console.WriteLine($"type => {command.value}");
                            break;
                        case "click":
                            we?.Click();
                            Console.WriteLine($"click => {command.targets[0][0].Split('=')[1]}");
                            break;
                    }
                    Console.WriteLine($"========================================");
                }
            }

            return this;
        }
    }
}
