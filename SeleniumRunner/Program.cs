﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumRunner.Engine;

namespace SeleniumRunner
{
    public class Program
    {
        static IWebDriver driver;
        static void Main(string[] args)
        {
            driver = new ChromeDriver();
            var x = new Engine.Engine(driver, @"C:\Users\User\Desktop\test side\testies.side");
            x.Open().Execute();
        }
    }
}
